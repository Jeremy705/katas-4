const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

function appendResults(newContent) {
    let newElement = document.createElement("div");
    newElement.textContent = JSON.stringify(newContent);
    document.body.appendChild(newElement);
}

function katas1() {
   let header = document.createElement("h1")
   header.innerHTML = "Katas 1"
   document.body.appendChild(header)
   let result = gotCitiesCSV.split()
   appendResults(result)
   return result
}
katas1()

function katas2() {
    let header = document.createElement("h1")
    header.innerHTML = "Katas 2"
    document.body.appendChild(header)
    let result = bestThing.split()
    appendResults(result)
    return result
}
katas2()

function katas3() {
    let header = document.createElement("h1")
    header.innerHTML = "Katas 3"
    document.body.appendChild(header)
    let result = gotCitiesCSV.split().join("; ")
    appendResults(result)
    return result
}
katas3()

function katas4() {
    let header = document.createElement("h1")
    header.innerHTML = "Katas 4"
    document.body.appendChild(header)
    let result = lotrCitiesArray.join(", ")
    appendResults(result)
}
katas4()

function katas5() {
    let header = document.createElement("h1")
    header.innerHTML = "Katas 5"
    document.body.appendChild(header)
    let result = lotrCitiesArray.slice(0, 5)
    appendResults(result)
    return result
}
katas5()

function katas6() {
    let header = document.createElement("h1")
    header.innerHTML = "Katas 6"
    document.body.appendChild(header)
    let result = lotrCitiesArray.slice(-5)
    appendResults(result)
    return result

}
katas6()

function katas7() {
    let header = document.createElement("h1")
    header.innerHTML = "Katas 7"
    document.body.appendChild(header)
    let result = lotrCitiesArray.slice(2, 5)
    appendResults(result)
    return result
}
katas7()

function katas8() {
    let header = document.createElement("h1")
    header.innerHTML = "Katas 8"
    document.body.appendChild(header)
    let result = lotrCitiesArray.splice(2, 1)
    appendResults(lotrCitiesArray) 
    return lotrCitiesArray   
}
katas8()

function katas9() {
    let header = document.createElement("h1")
    header.innerHTML = "Katas 9"
    document.body.appendChild(header)
    let result = lotrCitiesArray.splice(5)
    appendResults(lotrCitiesArray) 
}
katas9()

function katas10() {
    let header = document.createElement("h1")
    header.innerHTML = "Katas 10"
    document.body.appendChild(header)
    let result = lotrCitiesArray.splice(2, 0, "Rohan") 
    appendResults(lotrCitiesArray)   
}
katas10()

function katas11() {
    let header = document.createElement("h1")
    header.innerHTML = "Katas 11"
    document.body.appendChild(header)
    let result = lotrCitiesArray.splice(5, 1, 'Deadest Marshes')
    appendResults(lotrCitiesArray)
}
katas11()

function katas12() {
    let header = document.createElement("h1")
    header.innerHTML = "Katas 12"
    document.body.appendChild(header)
    let result = bestThing.slice(0, 14)
    appendResults(result)
    return result
}
katas12()

function katas13() {
    let header = document.createElement("h1")
    header.innerHTML = "Katas 13"
    document.body.appendChild(header)
    let result = bestThing.slice(-12)
    appendResults(result)
    return result
}
katas13()

function katas14() {
    let header = document.createElement("h1")
    header.innerHTML = "Katas 14"
    document.body.appendChild(header)
    let result = bestThing.slice(23, 39)
    appendResults(result)
    return result
}
katas14()

function katas15() {
    let header = document.createElement("h1")
    header.innerHTML = "Katas 15"
    document.body.appendChild(header)
    let result = bestThing.substring(bestThing.length - 12, bestThing.length)
    appendResults(result)
    return result
}
katas15()

function katas16(){
    let header = document.createElement("h1")
    header.innerHTML = "Katas 16"
    document.body.appendChild(header)
    let result = bestThing.substring(23, 39)
    appendResults(result)
    return result

}
katas16()

function katas17() {
    let header = document.createElement("h1")
    header.innerHTML = "Katas 17"
    document.body.appendChild(header)
    let result = lotrCitiesArray.pop()
    appendResults(lotrCitiesArray)
    return lotrCitiesArray
}
katas17()

function katas18(){
    let header = document.createElement("h1")
    header.innerHTML = "Katas 18"
    document.body.appendChild(header)
    let result = lotrCitiesArray.push("Dead Marshes")
    appendResults(lotrCitiesArray)
    return lotrCitiesArray
}
katas18()

function katas19() {
    let header = document.createElement("h1")
    header.innerHTML = "Katas 19"
    document.body.appendChild(header)
    let result = lotrCitiesArray.shift()
    appendResults(lotrCitiesArray)
    return lotrCitiesArray
}
katas19()

function katas20() {
    let header = document.createElement("h1")
    header.innerHTML = "Katas 20"
    document.body.appendChild(header)
    let result = lotrCitiesArray.unshift("Mordor")
    appendResults(lotrCitiesArray)
    return appendResults
}
katas20()



